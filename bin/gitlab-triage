#!/usr/bin/env ruby

require 'yaml'
require_relative '../lib/gitlab/triage/option_parser'
require_relative '../lib/gitlab/triage/engine'
require_relative '../lib/gitlab/triage/ui'

options = Gitlab::Triage::OptionParser.parse(ARGV)
options.policies_files << '.triage-policies.yml' if options.policies_files.empty?

options.policies_files.each do |policies_file|
  policies = if Psych::VERSION >= '4.0'
               HashWithIndifferentAccess.new(YAML.load_file(policies_file, aliases: true))
             else
               HashWithIndifferentAccess.new(YAML.load_file(policies_file))
             end

  policy_engine = Gitlab::Triage::Engine
    .new(policies: policies, options: options)

  puts Gitlab::Triage::UI.header("Executing policies from #{policies_file}.", char: '*')
  policy_engine.perform
end
