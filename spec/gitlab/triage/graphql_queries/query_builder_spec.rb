require 'spec_helper'

require 'gitlab/triage/graphql_queries/query_builder'

describe Gitlab::Triage::GraphqlQueries::QueryBuilder do
  let(:conditions) { {} }
  let(:query_builder) { described_class.new(source_type, resource_type, conditions, graphql_only: graphql_only) }
  let(:graphql_only) { false }

  describe '#resource_path' do
    subject { query_builder.resource_path }

    shared_examples 'resource path' do
      it 'prepares GraphQL query with proper source and resource types' do
        expect(subject).to eq([source_type.singularize, resource_type])
      end
    end

    context 'when source_type is projects' do
      let(:source_type) { 'projects' }

      context 'when resource_type is issues' do
        let(:resource_type) { 'issues' }

        it_behaves_like 'resource path'
      end

      context 'when resource_type is merge_requests' do
        let(:resource_type) { 'merge_requests' }

        it_behaves_like 'resource path'
      end
    end

    context 'when source_type is groups' do
      let(:source_type) { 'groups' }

      context 'when resource_type is issues' do
        let(:resource_type) { 'issues' }

        it_behaves_like 'resource path'
      end

      context 'when resource_type is merge_requests' do
        let(:resource_type) { 'merge_requests' }

        it_behaves_like 'resource path'
      end
    end
  end

  describe '#query' do
    subject(:graphql_query) { query_builder.query }

    let(:extra_conditions) { nil }
    let(:query) do
      <<~GRAPHQL
        query($source: ID!, $after: String%{extra_conditions}) {
          %{source_type}(fullPath: $source) {
            id
            %{resource_type}(after: $after%{group_query}%{resource_query}) {
              pageInfo {
                hasNextPage
                endCursor
              }
              nodes {
                id iid title updatedAt createdAt webUrl projectId %{fields}
              }
            }
          }
        }
      GRAPHQL
    end

    let(:formatted_query) do
      format(
        query,
        source_type: source_type.singularize,
        resource_type: resource_type.camelize(:lower),
        group_query: group_query,
        fields: resource_fields,
        resource_query: resource_query,
        extra_conditions: extra_conditions
      )
    end

    shared_examples 'graphql query' do
      let(:resource_fields) { fields }

      it 'prepares GraphQL query with proper source and resource types' do
        expect(graphql_query).to eq(formatted_query)
      end
    end

    shared_examples 'graphql only query' do
      let(:graphql_only) { true }

      let(:resource_fields) do
        resource_type == 'merge_requests' ? [fields, 'draft', 'mergedAt'].compact.join(' ') : fields
      end

      it 'prepares GraphQL query with proper source and resource types' do
        expect(graphql_query).to eq(formatted_query)
      end
    end

    shared_examples 'notes graphql query' do
      context 'when discussions->notes condition is provided' do
        let(:conditions) do
          {
            discussions: {
              attribute: :notes
            }
          }
        end

        let(:resource_query) { nil }
        let(:fields) { 'userNotesCount' }

        it_behaves_like 'graphql query'
      end
    end

    shared_examples 'threads graphql query' do
      context 'when discussions->threads condition is provided' do
        let(:conditions) do
          {
            discussions: {
              attribute: :threads
            }
          }
        end

        let(:resource_query) { nil }
        let(:fields) { 'userDiscussionsCount' }

        it_behaves_like 'graphql query'
      end
    end

    shared_examples 'assignees graphql query' do
      context 'when assignee_member condition is provided' do
        let(:conditions) do
          {
            assignee_member: {
              source: 'group',
              condition: 'not_member_of',
              source_id: 9970
            }
          }
        end

        let(:resource_query) { nil }
        let(:fields) { 'labels { nodes { title } } author { id name username } assignees { nodes { id name username } }' }

        it_behaves_like 'graphql only query'
      end
    end

    shared_examples 'author graphql query' do
      context 'when author_member condition is provided' do
        let(:conditions) do
          {
            author_member: {
              source: 'group',
              condition: 'not_member_of',
              source_id: 9970
            }
          }
        end

        let(:resource_query) { nil }
        let(:fields) { 'labels { nodes { title } } author { id name username }' }

        it_behaves_like 'graphql only query'
      end
    end

    shared_examples 'upvotes graphql query' do
      context 'when upvotes->upvotes condition is provided' do
        let(:conditions) do
          {
            upvotes: {
              attribute: 'upvotes',
              condition: 'less_than',
              threshold: 10
            }
          }
        end

        let(:resource_query) { nil }
        let(:fields) { 'labels { nodes { title } } author { id name username } upvotes' }

        it_behaves_like 'graphql only query'
      end
    end

    shared_examples 'downvotes graphql query' do
      context 'when upvotes->downvotes condition is provided' do
        let(:conditions) do
          {
            upvotes: {
              attribute: 'downvotes',
              condition: 'less_than',
              threshold: 10
            }
          }
        end

        let(:resource_query) { nil }
        let(:fields) { 'labels { nodes { title } } author { id name username } downvotes' }

        it_behaves_like 'graphql only query'
      end
    end

    shared_examples 'labels graphql query', :label_field do |label_field|
      context 'when labels condition is provided' do
        let(:conditions) do
          {
            labels: ['awaiting feedback']
          }
        end

        let(:resource_query) { ", #{label_field}: [\"awaiting feedback\"]" }
        let(:fields) { 'labels { nodes { title } } author { id name username }' }

        it_behaves_like 'graphql only query'
      end
    end

    shared_examples 'forbidden_labels graphql query', :label_field do |label_field|
      context 'when forbidden_labels condition is provided' do
        let(:conditions) do
          {
            forbidden_labels: ['awaiting feedback']
          }
        end

        let(:resource_query) { ", not: { #{label_field}: [\"awaiting feedback\"] }" }
        let(:fields) { 'labels { nodes { title } } author { id name username }' }

        it_behaves_like 'graphql only query'
      end
    end

    shared_examples 'no_additional_labels graphql query' do
      context 'when no_additional_labels condition is provided' do
        let(:conditions) do
          {
            no_additional_labels: true
          }
        end

        let(:resource_query) { nil }
        let(:fields) { 'labels { nodes { title } } author { id name username }' }

        it_behaves_like 'graphql only query'
      end
    end

    shared_examples 'date graphql query' do
      context 'when date-based condition is provided' do
        let(:conditions) do
          {
            date: {
              attribute: 'merged_at',
              condition: 'older_than',
              interval_type: 'days',
              interval: 5
            }
          }
        end

        let(:resource_query) { ', mergedBefore: "2020-05-10"' }
        let(:fields) { 'labels { nodes { title } } author { id name username }' }

        around do |example|
          travel_to(Time.utc(2020, 5, 15)) do
            example.run
          end
        end

        it_behaves_like 'graphql only query'
      end

      context 'when time-based condition is provided' do
        let(:conditions) do
          {
            date: {
              attribute: 'merged_at',
              condition: 'older_than',
              interval_type: 'hours',
              interval: 5
            }
          }
        end

        let(:time) { Time.local(2020, 5, 15, 9, 30) }
        let(:resource_query) { ", mergedBefore: \"#{(time - 5.hours).iso8601}\"" }
        let(:fields) { 'labels { nodes { title } } author { id name username }' }

        around do |example|
          travel_to(time) do
            example.run
          end
        end

        it_behaves_like 'graphql only query'
      end
    end

    shared_examples 'milestone graphql query' do
      context 'when milestone condition is provided' do
        let(:conditions) do
          {
            milestone: '14.0'
          }
        end

        let(:resource_query) { ', milestoneTitle: "14.0"' }
        let(:fields) { 'labels { nodes { title } } author { id name username }' }

        it_behaves_like 'graphql only query'
      end
    end

    shared_examples 'state graphql query' do
      context 'when state condition is provided' do
        let(:conditions) do
          {
            state: 'opened'
          }
        end

        let(:resource_query) { ', state: opened' }
        let(:fields) { 'labels { nodes { title } } author { id name username }' }

        it_behaves_like 'graphql only query'
      end
    end

    shared_examples 'issue_type graphql query' do
      context 'when issue_type condition is provided' do
        let(:conditions) do
          {
            issue_type: 'incident'
          }
        end

        let(:resource_query) { ', types: [INCIDENT]' }
        let(:fields) { 'labels { nodes { title } } author { id name username }' }

        it_behaves_like 'graphql only query'
      end
    end

    shared_examples 'source branch graphql query' do
      context 'when source branch condition is provided' do
        let(:conditions) do
          {
            source_branch: 'main'
          }
        end

        let(:resource_query) { ', sourceBranches: "main"' }
        let(:fields) { 'labels { nodes { title } } author { id name username }' }

        it_behaves_like 'graphql only query'
      end
    end

    shared_examples 'target branch graphql query' do
      context 'when target branch condition is provided' do
        let(:conditions) do
          {
            target_branch: 'main'
          }
        end

        let(:resource_query) { ', targetBranches: "main"' }
        let(:fields) { 'labels { nodes { title } } author { id name username }' }

        it_behaves_like 'graphql only query'
      end
    end

    shared_examples 'draft: true graphql query' do
      context 'when draft: true condition is provided' do
        let(:conditions) do
          {
            draft: true
          }
        end

        let(:resource_query) { ', draft: true' }
        let(:fields) { 'labels { nodes { title } } author { id name username }' }

        it_behaves_like 'graphql only query'
      end
    end

    shared_examples 'draft: false graphql query' do
      context 'when draft: false condition is provided' do
        let(:conditions) do
          {
            draft: false
          }
        end

        let(:resource_query) { ', draft: false' }
        let(:fields) { 'labels { nodes { title } } author { id name username }' }

        it_behaves_like 'graphql only query'
      end
    end

    shared_examples 'iids graphql query' do
      context 'when iids condition is provided' do
        let(:conditions) do
          {
            iids: '42'
          }
        end

        let(:extra_conditions) { ', $iids: [String!]' }
        let(:resource_query) { ', iids: $iids' }
        let(:fields) { 'labels { nodes { title } } author { id name username }' }

        it_behaves_like 'graphql only query'
      end
    end

    shared_examples 'graphql queries for different resource types' do
      context 'when resource_type is issues' do
        let(:resource_type) { 'issues' }

        it_behaves_like 'notes graphql query'
        it_behaves_like 'threads graphql query'
        it_behaves_like 'iids graphql query'
        it_behaves_like 'assignees graphql query'
        it_behaves_like 'author graphql query'
        it_behaves_like 'upvotes graphql query'
        it_behaves_like 'downvotes graphql query'
        it_behaves_like 'labels graphql query', 'labelName'
        it_behaves_like 'forbidden_labels graphql query', 'labelName'
        it_behaves_like 'no_additional_labels graphql query'
        it_behaves_like 'date graphql query'
        it_behaves_like 'milestone graphql query'
        it_behaves_like 'state graphql query'
        it_behaves_like 'issue_type graphql query'
      end

      context 'when resource_type is merge_requests' do
        let(:resource_type) { 'merge_requests' }

        it_behaves_like 'notes graphql query'
        it_behaves_like 'threads graphql query'
        it_behaves_like 'iids graphql query'
        it_behaves_like 'assignees graphql query'
        it_behaves_like 'author graphql query'
        it_behaves_like 'upvotes graphql query'
        it_behaves_like 'downvotes graphql query'
        it_behaves_like 'labels graphql query', 'labels'
        it_behaves_like 'forbidden_labels graphql query', 'labels'
        it_behaves_like 'no_additional_labels graphql query'
        it_behaves_like 'date graphql query'
        it_behaves_like 'milestone graphql query'
        it_behaves_like 'state graphql query'
        it_behaves_like 'source branch graphql query'
        it_behaves_like 'target branch graphql query'
        it_behaves_like 'draft: true graphql query'
        it_behaves_like 'draft: false graphql query'
      end
    end

    context 'when source_type is projects' do
      let(:source_type) { 'projects' }
      let(:group_query) { nil }

      it_behaves_like 'graphql queries for different resource types'
    end

    context 'when source_type is groups' do
      let(:source_type) { 'groups' }
      let(:group_query) { ', includeSubgroups: true' }

      it_behaves_like 'graphql queries for different resource types'
    end
  end
end
