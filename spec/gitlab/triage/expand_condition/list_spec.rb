require 'spec_helper'

require 'gitlab/triage/expand_condition/list'

describe Gitlab::Triage::ExpandCondition::List do
  describe '.expand' do
    it 'expands the conditions for labels' do
      conditions = subject.expand(
        labels: %w[buy:{apple,orange} sell:{grape,banana} fruits]
      )

      expect(conditions).to eq(
        [
          { labels: %w[buy:apple sell:grape fruits] },
          { labels: %w[buy:apple sell:banana fruits] },
          { labels: %w[buy:orange sell:grape fruits] },
          { labels: %w[buy:orange sell:banana fruits] }
        ]
      )
    end

    it 'expands the conditions for labels, ignoring spaces' do
      conditions = subject.expand(
        labels: ['{ apple , orange }']
      )

      expect(conditions).to eq(
        [
          { labels: %w[apple] },
          { labels: %w[orange] }
        ]
      )
    end

    it 'expands the conditions for labels, ignoring newlines' do
      conditions = subject.expand(
        labels: ["{\n apple\n , \norange \n}\n"]
      )

      expect(conditions).to eq(
        [
          { labels: %w[apple] },
          { labels: %w[orange] }
        ]
      )
    end
  end
end
