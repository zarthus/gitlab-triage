require 'spec_helper'

require 'gitlab/triage/filters/branch_date_filter'

describe Gitlab::Triage::Filters::BranchDateFilter do
  let(:committed_date) { Date.new(2016, 1, 31) }
  let(:authored_date) { Date.new(2017, 1, 1) }

  let(:resource) do
    {
      commit: {
        committed_date: committed_date,
        authored_date: authored_date
      }
    }
  end
  let(:condition) do
    {
      attribute: 'committed_date',
      condition: 'older_than',
      interval_type: 'months',
      interval: 3
    }
  end

  let(:condition_newer) do
    {
      attribute: 'committed_date',
      condition: 'newer_than',
      interval_type: 'months',
      interval: 3
    }
  end

  let(:condition_authored_date) do
    {
      attribute: 'authored_date',
      condition: 'newer_than',
      interval_type: 'months',
      interval: 3
    }
  end

  subject { described_class.new(resource, condition) }

  it_behaves_like 'a filter'
  it_behaves_like 'an issuable date filter'

  describe '#resource_value' do
    it 'has the correct value for the committed_date attribute' do
      filter = described_class.new(resource, condition)
      expect(filter.resource_value).to eq(committed_date)
    end

    it 'has the correct value for the authored_date attribute' do
      filter = described_class.new(resource, condition_authored_date)
      expect(filter.resource_value).to eq(authored_date)
    end
  end

  describe '#condition_value' do
    let(:time) { Time.local(2022, 11, 26, 9, 30) }

    around do |example|
      travel_to(time) do
        example.run
      end
    end

    describe 'when date-based condition is provided' do
      it 'has the correct value for the codition value filter' do
        filter = described_class.new(resource, condition)
        expect(filter.condition_value).to eq((time - 3.months).to_date)
      end
    end

    describe 'when time-based condition is provided' do
      let(:condition) do
        {
          attribute: 'authored_date',
          condition: 'newer_than',
          interval_type: 'hours',
          interval: 3
        }
      end

      it 'has the correct value for the codition value filter' do
        filter = described_class.new(resource, condition)
        expect(filter.condition_value).to eq((time - 3.hours).iso8601)
      end
    end
  end

  describe '#calculate ' do
    it 'calculates true given correct condition' do
      expect(subject.calculate).to eq(true)
    end

    it 'calculates false given incorrect condition' do
      filter = described_class.new(resource, condition_newer)
      expect(filter.calculate).to eq(false)
    end
  end
end
