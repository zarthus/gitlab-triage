# frozen_string_literal: true

require 'spec_helper'

shared_examples 'select resources by votes' do
  include_context 'with integration context'

  let!(:resource_with_no_vote) do
    public_send(resource_type).merge(upvotes: 0, downvotes: 0)
  end
  let!(:resource_with_2_upvotes) do
    public_send(resource_type).merge(iid: public_send(resource_type)[:iid] * 2, upvotes: 2, downvotes: 0)
  end
  let!(:resource_with_3_downvotes) do
    public_send(resource_type).merge(iid: public_send(resource_type)[:iid] * 3, upvotes: 0, downvotes: 3)
  end

  context 'with no vote' do
    before do
      stub_api(
        :get,
        "https://gitlab.com/api/v4/projects/#{project_id}/#{resource_type}s",
        query: { per_page: 100 },
        headers: { 'PRIVATE-TOKEN' => token }) do
        [resource_with_no_vote]
      end
    end

    it 'does not comment on the resource' do
      rule = <<~YAML
        resource_rules:
          #{resource_type}s:
            rules:
              - name: Test rule
                conditions:
                  votes:
                    attribute: upvotes
                    condition: greater_than
                    threshold: 0
                actions:
                  comment: |
                    Comment because it has at least an upvote.
      YAML

      perform(rule)
    end
  end

  shared_examples 'matching resource' do
    before do
      stub_api(
        :get,
        "https://gitlab.com/api/v4/projects/#{project_id}/#{resource_type}s",
        query: { per_page: 100 },
        headers: { 'PRIVATE-TOKEN' => token }) do
        [matching_resource]
      end
    end

    it 'comments on the resource' do
      rule = <<~YAML
        resource_rules:
          #{resource_type}s:
            rules:
              - name: Test rule
                conditions:
                  #{condition_key}:
                    attribute: #{vote_attribute}
                    condition: greater_than
                    threshold: 1
                actions:
                  comment: |
                    Comment because it matches the vote condition.
      YAML

      stub_post_matching_resource = stub_api(
        :post,
        "https://gitlab.com/api/v4/projects/#{project_id}/#{resource_type}s/#{matching_resource[:iid]}/notes",
        body: { body: 'Comment because it matches the vote condition.' },
        headers: { 'PRIVATE-TOKEN' => token })

      perform(rule)

      assert_requested(stub_post_matching_resource)
    end
  end

  it_behaves_like 'matching resource' do
    let(:condition_key) { :votes }
    let(:vote_attribute) { :upvotes }
    let(:matching_resource) { resource_with_2_upvotes }
  end

  it_behaves_like 'matching resource' do
    let(:condition_key) { :votes }
    let(:vote_attribute) { :downvotes }
    let(:matching_resource) { resource_with_3_downvotes }
  end

  # Back-compatibility
  it_behaves_like 'matching resource' do
    let(:condition_key) { :upvotes }
    let(:vote_attribute) { :upvotes }
    let(:matching_resource) { resource_with_2_upvotes }
  end

  # Back-compatibility
  it_behaves_like 'matching resource' do
    let(:condition_key) { :upvotes }
    let(:vote_attribute) { :downvotes }
    let(:matching_resource) { resource_with_3_downvotes }
  end
end

describe 'select issues by votes' do
  it_behaves_like 'select resources by votes' do
    let(:resource_type) { :issue }
  end
end

describe 'select merge_requests by votes' do
  it_behaves_like 'select resources by votes' do
    let(:resource_type) { :merge_request }
  end
end
