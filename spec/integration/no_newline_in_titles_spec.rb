# frozen_string_literal: true

require 'spec_helper'

describe 'Translate newlines to spaces in titles' do
  include_context 'with integration context'

  before do
    issue[:title] = "issue\r\ntitle"

    stub_api(
      :get,
      "https://gitlab.com/api/v4/projects/#{project_id}/issues",
      query: { per_page: 100 },
      headers: { 'PRIVATE-TOKEN' => token }) do
      issues
    end
  end

  def stub_and_perform_summary(rule, expected_title, expected_description)
    stub_post = stub_api(
      :post,
      "https://gitlab.com/api/v4/projects/#{project_id}/issues",
      body: { title: expected_title, description: expected_description },
      headers: { 'PRIVATE-TOKEN' => token })

    perform(rule)

    assert_requested(stub_post)
  end

  it 'creates a summary issue with translated titles' do
    rule = <<~YAML
      resource_rules:
        issues:
          rules:
            - name: Rule name
              actions:
                summarize:
                  item: |
                    - {{title}}
                  title: |
                    Summary for \#{resource[:type]}
                  summary: |
                    Start summary

                    {{items}}

                    End summary
    YAML

    expected_title = 'Summary for issues'
    expected_description = <<~MARKDOWN.chomp
    Start summary

    - issue  title

    End summary
    MARKDOWN

    stub_and_perform_summary(rule, expected_title, h(expected_description))
  end
end
