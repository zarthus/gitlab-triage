# frozen_string_literal: true

require 'spec_helper'

describe 'select issues by issue type' do
  include_context 'with integration context'

  let(:regular_issue) do
    issue.merge(issue_type: 'issue')
  end
  let(:incident_issue) do
    issue.merge(issue_type: 'incident', iid: issue[:iid] + 1)
  end
  let(:test_case_issue) do
    issue.merge(issue_type: 'test_case', iid: issue[:iid] + 2)
  end

  let(:issue_type) { regular_issue[:issue_type] }

  before do
    stub_issues_api_with_query(issue_type, resource)
  end

  shared_examples 'matched rule' do
    it 'posts the correct comment on the issue' do
      stub_post = stub_api_comment(resource[:iid], comment)

      perform(rule(issue_type, comment))

      assert_requested(stub_post)
    end
  end

  context 'with regular issue' do
    let(:resource) { regular_issue }
    let(:comment) { 'Comment because it is a regular issue.' }

    it_behaves_like 'matched rule'
  end

  context 'with incident issue' do
    let(:resource) { incident_issue }
    let(:comment) { 'Comment because it is an incident issue.' }

    it_behaves_like 'matched rule'
  end

  context 'with test case issue' do
    let(:resource) { test_case_issue }
    let(:comment) { 'Comment because it is a test case issue.' }

    it_behaves_like 'matched rule'
  end

  def stub_api_comment(iid, body)
    stub_api(
      :post,
      "https://gitlab.com/api/v4/projects/#{project_id}/issues/#{iid}/notes",
      body: { body: body },
      headers: { 'PRIVATE-TOKEN' => token })
  end

  def stub_issues_api_with_query(issue_type, issue)
    stub_api(
      :get,
      "https://gitlab.com/api/v4/projects/#{project_id}/issues",
      query: { per_page: 100, issue_type: issue_type },
      headers: { 'PRIVATE-TOKEN' => token }) do
        [issue]
      end
  end

  def rule(issue_type, comment)
    <<~YAML
      resource_rules:
        issues:
          rules:
            - name: Test rule
              conditions:
                issue_type: #{issue_type}
              actions:
                comment: |
                  #{comment}
    YAML
  end
end
