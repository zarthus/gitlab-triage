# frozen_string_literal: true

require 'spec_helper'

describe 'redact confidential issues in the summary' do
  include_context 'with integration context'

  before do
    issue[:confidential] = true

    stub_api(
      :get,
      "https://gitlab.com/api/v4/projects/#{project_id}/issues",
      query: { per_page: 100 },
      headers: { 'PRIVATE-TOKEN' => token }) do
      issues
    end
  end

  def stub_and_perform_summary(rule, expected_title, expected_description)
    stub_post = stub_api(
      :post,
      "https://gitlab.com/api/v4/projects/#{project_id}/issues",
      body: { title: expected_title, description: expected_description },
      headers: { 'PRIVATE-TOKEN' => token })

    perform(rule)

    assert_requested(stub_post)
  end

  def stub_and_perform_action(rule, expected_comment)
    stub_post = stub_api(
      :post,
      "https://gitlab.com/api/v4/projects/#{project_id}/issues/#{issue[:iid]}/notes",
      body: { body: expected_comment },
      headers: { 'PRIVATE-TOKEN' => token })

    perform(rule)

    assert_requested(stub_post)
  end

  context 'when using summary' do
    it 'creates a summary issue with redacted title and labels' do
      rule = <<~YAML
        resource_rules:
          issues:
            rules:
              - name: Rule name
                actions:
                  summarize:
                    item: |
                      - [ ] [{{title}}]({{web_url}}) {{labels}}
                    title: |
                      Summary for \#{resource[:type]}
                    summary: |
                      Start summary
  
                      {{items}}
  
                      End summary
      YAML

      expected_title = 'Summary for issues'
      expected_description = <<~MARKDOWN.chomp
      Start summary
  
      - [ ] [(confidential)](#{issue[:web_url]}) ~"(confidential)"
  
      End summary
      MARKDOWN

      stub_and_perform_summary(rule, expected_title, h(expected_description))
    end

    context 'when redact_confidential_resources is set to false' do
      it 'creates a summary issue with revealed title and labels' do
        rule = <<~YAML
          resource_rules:
            issues:
              rules:
                - name: Rule name
                  actions:
                    summarize:
                      item: |
                        - [ ] [{{title}}]({{web_url}}) {{labels}}
                      redact_confidential_resources: false
                      title: |
                        Summary for {{type}}
                      summary: |
                        Start summary
  
                        {{items}}
  
                        End summary
        YAML

        expected_title = 'Summary for issues'
        expected_description = <<~MARKDOWN.chomp
        Start summary
  
        - [ ] [#{issue[:title]}](#{issue[:web_url]}) ~"#{issue.dig(:labels, 0)}"
  
        End summary
        MARKDOWN

        stub_and_perform_summary(rule, expected_title, h(expected_description))
      end
    end
  end

  context 'when using action' do
    it 'when redact_confidential_resources is false' do
      rule = <<~YAML
        resource_rules:
          issues:
            rules:
              - name: Rule name
                actions:
                  redact_confidential_resources: false
                  comment: |
                    Hey {{author}}
      YAML

      expected_comment = <<~MARKDOWN.chomp
        Hey @#{issue.dig(:author, :username)}
      MARKDOWN

      stub_and_perform_action(rule, expected_comment)
    end

    it 'when redact_confidential_resources is true' do
      rule = <<~YAML
        resource_rules:
          issues:
            rules:
              - name: Rule name
                actions:
                  redact_confidential_resources: true
                  comment: |
                    Hey {{author}}
      YAML

      expected_comment = <<~MARKDOWN.chomp
        Hey @(confidential)
      MARKDOWN

      stub_and_perform_action(rule, expected_comment)
    end

    it 'when redact_confidential_resources is not set' do
      rule = <<~YAML
        resource_rules:
          issues:
            rules:
              - name: Rule name
                actions:
                  comment: |
                    Hey {{author}}
      YAML

      expected_comment = <<~MARKDOWN.chomp
        Hey @(confidential)
      MARKDOWN

      stub_and_perform_action(rule, expected_comment)
    end
  end
end
